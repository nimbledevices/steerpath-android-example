# Copyright Steerpath Ltd. 2016. All rights reserved #

Steerpath 3 Android SDK Examples.

### What is this repository for? ###

* Steerpath Android SDK examples for indoor positioning.
* Version 1.0.0

### API Keys and Access Tokens for the SDK ###

See com.steerpath.example.ExampleApplication for SDK initialization:

Latest SDK uses Steerpath Maps.

* Steerpath API Key
* [Optional] Telemetry configuration
* [Optional] OfflineBundle configuration



Legacy SDK uses Mapbox Maps. To use legacy SDK, checkout tag 1.0.0

* Steerpath API Key
* Mapbox Access Token
* Map style url
* [Optional] Telemetry configuration
* [Optional] OfflineBundle configuration

### Contact ###

* support@steerpath.com