package com.steerpath.example.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.steerpath.sdk.location.LocationRequest;
import com.steerpath.sdk.location.LocationSource;

public class CustomLocationSource implements LocationSource {

    private final LocationRequest request;
    private OnLocationChangedListener listener;
    private LocationListener internalLocationListener;
    private final Object listenerLock = new Object();

    public CustomLocationSource(Context context, final LocationRequest req) {
        this.request = req;
    }

    public CustomLocationSource(Context context) {
        this(context, LocationRequest.create());
    }

    @Override
    public void activate(final OnLocationChangedListener listener){

        CustomLocationProvider.getInstance().requestLocationUpdates(listener);
        synchronized (listenerLock) {
            this.listener = listener;
        }

        internalLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                synchronized (listenerLock) {
                    if (CustomLocationSource.this.listener != null) {
                        CustomLocationSource.this.listener.onLocationChanged(location);
                    }
                }
            }

            public void onStatusChanged(String s, int i, Bundle bundle) {}
            public void onProviderEnabled(String s) {}
            public void onProviderDisabled(String s) {}
        };
    }

    @Override
    public void deactivate() {
        if (internalLocationListener != null) {
            internalLocationListener = null;
            synchronized (listenerLock) {
                CustomLocationProvider.getInstance().removeLocationUpdates(listener);
                listener = null;
            }
        }
    }
}