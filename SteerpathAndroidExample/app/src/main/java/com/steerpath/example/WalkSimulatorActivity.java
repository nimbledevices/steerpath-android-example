package com.steerpath.example;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.steerpath.example.utils.AnnotationOptionsFactory;
import com.steerpath.example.utils.MapHelper;
import com.steerpath.sdk.directions.DirectionsException;
import com.steerpath.sdk.directions.DirectionsResponse;
import com.steerpath.sdk.directions.Route;
import com.steerpath.sdk.directions.RouteListener;
import com.steerpath.sdk.directions.RoutePlan;
import com.steerpath.sdk.directions.RouteStep;
import com.steerpath.sdk.directions.RouteTrackerProgress;
import com.steerpath.sdk.directions.RouteUtils;
import com.steerpath.sdk.directions.Waypoint;
import com.steerpath.sdk.location.FusedLocationProviderApi;
import com.steerpath.sdk.maps.OnMapReadyCallback;
import com.steerpath.sdk.maps.RoutePreviewHolder;
import com.steerpath.sdk.maps.RouteStepViewHolder;
import com.steerpath.sdk.maps.SteerpathAnnotation;
import com.steerpath.sdk.maps.SteerpathAnnotationOptions;
import com.steerpath.sdk.maps.SteerpathMap;
import com.steerpath.sdk.maps.SteerpathMapFragment;
import com.steerpath.sdk.maps.SteerpathMapView;
import com.steerpath.sdk.maps.defaults.DirectionsAssetHelper;
import com.steerpath.sdk.meta.MetaFeature;
import com.steerpath.sdk.utils.CompassUtils;
import com.steerpath.sdk.utils.internal.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.steerpath.example.ExampleApplication.EXTRAS_HELPER_BUILDING;

/**
 * Created by opiirone on 17/05/2018.
 */

public class WalkSimulatorActivity extends AppCompatActivity implements SteerpathMapFragment.MapViewListener {

    private TextView info = null;
    private SteerpathMap map = null;
    private SteerpathMapView mapView = null;
    private View progressBar = null;

    private List<SteerpathAnnotation> anns = new ArrayList<>();
    private MetaFeature selectedPOI = null;

    private List<Location> locations = new ArrayList<>();
    private Location currentLocation = null;
    private Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_w_info);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        info = (TextView) findViewById(R.id.info);
        info.setText("Touch map to set Location");
        progressBar = findViewById(R.id.steerpath_map_progressbar);

        // http://stackoverflow.com/questions/22926393/why-is-my-oncreateview-method-being-called-twice
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_parent, SteerpathMapFragment.newInstance(), "steerpath-map-fragment").commit();
        }

        MetaFeature building = getIntent().getParcelableExtra(EXTRAS_HELPER_BUILDING);
        getSupportActionBar().setSubtitle(building.getTitle());

        // enable Fake Location for this Activity. See also onBackPressed()
        FusedLocationProviderApi.Api.get().enableFakeLocation(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // disable Fake Location after no longer needed. Otherwise positioning does not work in other Activities!
        FusedLocationProviderApi.Api.get().enableFakeLocation(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.simple_navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear_all_annotations:
                restart();
                return true;

            case R.id.get_directions:
                if (selectedPOI != null) {
                    showRoutePreview(selectedPOI);
                } else {
                    Toast.makeText(this, "Select destination first", Toast.LENGTH_SHORT).show();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void restart() {
        handler.removeCallbacksAndMessages(null);
        info.setText("Touch map to set Location");
        setupFakeLocationUpdateOnClickListener();
        clearAllAnnotations();
        locations.clear();
    }

    @Override
    public void onMapViewReady(final SteerpathMapView mapView) {
        this.mapView = mapView;

        setupCustomUI();

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final SteerpathMap steerpathMap) {
                map = steerpathMap;

                // move camera somewhere meaningful place
                MetaFeature building = getIntent().getParcelableExtra(EXTRAS_HELPER_BUILDING);
                MapHelper.moveCameraTo(map, building);

                map.setMyLocationEnabled(true);

                // we are using Fake Location. Configure map to use that LocationSource
                map.setLocationSource(FusedLocationProviderApi.Api.get().getFakeLocationSource());
                setupFakeLocationUpdateOnClickListener();
            }
        });
    }

    private void setupFakeLocationUpdateOnClickListener() {
        mapView.setOnMapClickListener(new MapboxMap.OnMapClickListener() {
            @Override
            public void onMapClick(@NonNull LatLng point) {
                FusedLocationProviderApi.Api.get().getFakeLocationSource().setLocation(Utils.toFakeLocation(point, map));
                info.setText("Click POI to select destination");
                setupOnMapClickListener();
            }
        });
    }

    /**
     * Change appearance of Route Preview Bagde, Step-by-Step Badge and Directions List
     */
    private void setupCustomUI() {

        // route preview badge
        mapView.setRoutePreviewHolder(new MyRoutePreviewHolder(this));

        // step-by-step badge
        RouteStepViewHolder peekBadge = new MyRouteStepViewHolder(this);
        mapView.setRouteStepViewHolder(peekBadge);

        mapView.setCompassCalibrationDialogEnabled(false);
//        mapView.setCompassCalibrationRecommendedListener(new CompassUtils.Listener() {
//            @Override
//            public void onCompassCalibrationRecommended(boolean calibrationRecommended) {
//                // show your custom dialog?
//            }
//        });
    }

    private void setupOnMapClickListener() {
        mapView.setOnMapClickListener(new MapboxMap.OnMapClickListener() {
            @Override
            public void onMapClick(@NonNull LatLng point) {

                MetaFeature feature = mapView.getRenderedMetaFeature(point);
                if (feature != null) {
                    // allow only one POI at the time
                    map.removeAnnotations(anns);
                    anns.clear();

                    SteerpathAnnotationOptions option = AnnotationOptionsFactory.createAnnotationOptions(mapView.getContext(), feature);
                    anns.add(map.addAnnotation(option));
                    selectedPOI = feature;

                    info.setText("Press Directions-button in ToolBar");
                }
            }
        });
    }

    private void clearAllAnnotations() {
        map.removeAnnotations(anns);
        anns.clear();
        mapView.stopNavigation();
        selectedPOI = null;
    }

    private void showRoutePreview(MetaFeature destination) {
        RoutePlan plan = new RoutePlan.Builder()
                .destination(destination)
                .rerouteThreshold(3)
                .goalDistanceThreshold(3)
                .build();

        progressBar.setVisibility(VISIBLE);
        mapView.previewRoute(plan, new RouteListener() {

            @Override
            public void onDirections(DirectionsResponse directions) {
                progressBar.setVisibility(GONE);
                extractLocations(directions);
                info.setText("Press Start");
            }

            @Override
            public void onProgress(RouteTrackerProgress progress) {

            }

            @Override
            public void onStepEntered(RouteStep step) {

            }

            @Override
            public void onWaypointReached(Waypoint waypoint) {

            }

            @Override
            public void onDestinationReached() {
                // OPTIONAL:
                //mapView.stopNavigation();

                info.setText("Destination reached!");
            }

            @Override
            public void onNavigationStopped() {

            }

            @Override
            public void onError(DirectionsException error) {
                progressBar.setVisibility(GONE);
                Toast.makeText(WalkSimulatorActivity.this, "Routing failed!", Toast.LENGTH_LONG).show();
                restart();
            }
        });
    }

    private void extractLocations(DirectionsResponse directions) {
        Route route = directions.getRoutes().get(0); // currently we have only one Route
        locations.clear(); // onDirections() is called twice
        for (RouteStep step : route.getSteps()) {
            for (LatLng latLng : step.getCoordinates()) {
                locations.add(Utils.toFakeLocation(latLng, step.getBuildingRef(), step.getFloor()));
            }

            // to make simulation faster, select LatLng of last segment
//            LatLng latLng = step.getCoordinates().get(step.getCoordinates().size()-1);
//            locations.add(Utils.toFakeLocation(latLng, step.getBuildingRef(), step.getFloor()));
        }
    }

    private void startWalkSimulation() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (currentLocation == null) {
                    currentLocation = locations.get(0);
                    // loop
                    handler.postDelayed(this, 2000);

                } else {
                    int index = locations.indexOf(currentLocation);
                    index++;
                    if (index < locations.size()) {
                        currentLocation = locations.get(index);
                        // loop
                        handler.postDelayed(this, 2000);
                    } else {
                        // we're done, no more looping
                    }
                }

                FusedLocationProviderApi.Api.get().getFakeLocationSource().setLocation(currentLocation);
            }
        });
    }

    private static class MyRoutePreviewHolder implements RoutePreviewHolder {

        private final WeakReference<WalkSimulatorActivity> ref;

        private MyRoutePreviewHolder(WalkSimulatorActivity parent) {
            this.ref = new WeakReference(parent);
        }

        @Override
        public View create(Context context, RoutePlan plan, DirectionsResponse directions) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup root = (ViewGroup)inflater.inflate(R.layout.custom_route_preview_badge, null, false);

            Button startButton = (Button) root.findViewById(R.id.route_preview_start_button);
            startButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ref.get() != null) {
                        ref.get().mapView.acceptRoute();
                        ref.get().info.setText("Walking now...");
                        ref.get().startWalkSimulation();
                    }
                }
            });

            ImageButton cancelButton = (ImageButton) root.findViewById(R.id.route_preview_cancel_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ref.get() != null) {
                        ref.get().mapView.stopNavigation();
                    }
                }
            });

            // generate RouteTrackerProgress object and calculate total distance
            List<RouteStep> steps = directions.getRoutes().get(0).getSteps();
            RouteStep first = steps.get(0);
            RouteTrackerProgress progress = RouteUtils.createRouteTrackerProgress(steps, first);
            TextView distanceInfo = (TextView)root.findViewById(R.id.route_preview_distance);
            distanceInfo.setText(DirectionsAssetHelper.getComposer().compose(context, progress));

            return root;
        }

        @Override
        public boolean useInBuiltActionButton() {
            return false;
        }
    }

    // example how to make custom route step badge
    private static class MyRouteStepViewHolder implements RouteStepViewHolder {
        private final WeakReference<WalkSimulatorActivity> ref;
        private ImageView image;
        private TextView stepInfo;
        private TextView distanceInfo;
        private DirectionsResponse directions;
        // RouteTrackerProgress does not carry "action" information but RouteStep does. Keep it for later updates.
        private RouteStep latestRouteStep;

        private MyRouteStepViewHolder(WalkSimulatorActivity parent) {
            this.ref = new WeakReference(parent);
        }

        @Override
        public View create(Context context, DirectionsResponse directions, RouteStep routeStep) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup stepBadge = (ViewGroup)inflater.inflate(R.layout.custom_route_step_badge, null, false);
            image = (ImageView)stepBadge.findViewById(R.id.my_step_badge_image);
            stepInfo = (TextView)stepBadge.findViewById(R.id.my_step_badge_directions);
            distanceInfo = (TextView)stepBadge.findViewById(R.id.my_step_badge_distance);
            Button stopButton = (Button) stepBadge.findViewById(R.id.my_step_badge_stop_button);
            stopButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ref.get() != null) {
                        ref.get().mapView.stopNavigation();
                        ref.get().restart();
                    }
                }
            });
            this.directions = directions;
            latestRouteStep = routeStep;
            return stepBadge;
        }

        @Override
        public void onReRoute(Context context, DirectionsResponse directions) {
            this.directions = directions;
        }

        @Override
        public void update(Context context, RouteStep step) {
            // NOTE: you may use in-built helpers to obtain proper icon and text for this RouteStep.
            latestRouteStep = step;
            image.setImageResource(DirectionsAssetHelper.getIconChooser().getDrawable(context, step));
            stepInfo.setText(DirectionsAssetHelper.getComposer().compose(context, step));
        }

        @Override
        public void update(Context context, RouteTrackerProgress progress) {
            // NOTE: you may use in-built helpers to obtain proper icon and text for this RouteTrackerProgress.
            stepInfo.setText(DirectionsAssetHelper.getComposer().compose(context, latestRouteStep, progress));
            distanceInfo.setText(DirectionsAssetHelper.getComposer().compose(context, directions, latestRouteStep, progress));
        }

        @Override
        public void onDestinationReached(Context context) {
            // NOTE: you may use in-built helpers to obtain proper icon and text when user has reached destination
            // yay were are here!
            stepInfo.setText(DirectionsAssetHelper.getComposer().getDestinationReachedMessage(context));
            image.setImageResource(DirectionsAssetHelper.getIconChooser().getDestinationReachedDrawableRes());
            distanceInfo.setText("");
        }

        @Override
        public boolean useInBuiltActionButton() {
            return false;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        restart();
    }
}
