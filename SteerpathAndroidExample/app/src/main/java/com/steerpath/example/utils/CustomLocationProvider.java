package com.steerpath.example.utils;

import android.location.Location;
import android.os.Handler;

import com.steerpath.sdk.location.LocationSource;
import com.steerpath.sdk.utils.internal.Utils;

import java.util.ArrayList;

public class CustomLocationProvider {

    private static CustomLocationProvider INSTANCE;

    private ArrayList<LocationSource.OnLocationChangedListener> listeners = new ArrayList<>();
    private ArrayList<Location> customLocations;

    private int counter = 0;

    private Handler handler = new Handler();

    /*
    * Sends fake location data to CustomLocationSource's listeners every 5s.
    */
    private Runnable looper = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(this, 5000);
            if (counter <= customLocations.size() -1) {
                Location nextLocation = customLocations.get(counter);
                for (LocationSource.OnLocationChangedListener listener : listeners) {
                    listener.onLocationChanged(nextLocation);
                }
                counter++;
            } else {
                counter = 0;
            }
        }
    };

    public static CustomLocationProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CustomLocationProvider();
        }
        return INSTANCE;
    }

    /*
     * Starts looping fake locations when first LocationSource is requesting location updates
     */
    public void requestLocationUpdates(LocationSource.OnLocationChangedListener listener) {
        if (listeners.isEmpty()) {
            createCustomLocations();
            handler.post(looper);
        }
        listeners.add(listener);
    }

    /*
    * Fake location data to be sent to CustomLocationSource location listeners
    */
    private ArrayList<Location> createCustomLocations() {
        customLocations = new ArrayList<>();
        customLocations.add(Utils.createLocation("office", 60.22100974415153, 24.812457766058287, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.220985857337865, 24.812414134925348, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.22098250301892, 24.81236405699292, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.221006084894185, 24.81228669520351, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.220957091495194, 24.812332859002368, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.220884211139435, 24.812299473708663, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.220916128080404, 24.81237153989707, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.22090474370023, 24.812422653934163, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.220945097250535, 24.81244199438163, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.22090281241981, 24.812483898684263, "67", 2));
        customLocations.add(Utils.createLocation("office", 60.22094977297647, 24.812433820735464, "67", 2));

        return customLocations;
    }

    public synchronized void removeLocationUpdates(LocationSource.OnLocationChangedListener listener) {
        listeners.remove(listener);
        stopRequest();
    }

    private synchronized void stopRequest() {
        if (listeners.size() != 0) {
            return;
        } else {
            handler.removeCallbacksAndMessages(null);
            INSTANCE = null;
        }
    }
}
