package com.steerpath.example.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;

import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.PropertyValue;
import com.steerpath.example.R;
import com.steerpath.sdk.maps.defaults.DefaultRouteOptionsFactory;

/**
 * Uses LineLayers and SymbolLayers for rendering.
 */

public class PreviewRouteOptionsFactory extends DefaultRouteOptionsFactory {

    @Override
    public int getRouteLineMode() {
        // show route segments from each floor
        return ROUTELINE_MODE_FULL;

        // just like ROUTELINE_MODE_FULL, but applied only if origin and destination are in different buildings.
        //return ROUTELINE_MODE_FULL_BETWEEN_BUILDINGS;
    }

    @Override
    public int getMarkerRenderMode() {
        return MARKER_RENDER_MODE_SYMBOL_LAYER;
    }

    @Override
    public int getLineRenderMode() {
        return LINE_RENDER_MODE_LINE_LAYER;
    }

    @Override
    public PropertyValue[] createRouteLineProperties(Context context, int routeLineIndex, int floorSegmentIndex) {

        // Assume route takes place in three floors. Three RouteLines are created, one per each floor.
        // When floor is selected, only corresponding RouteLine is rendered.
        //
        // ======================
        // RouteLine index 2        ----> this is same than "selected floor index"
        // ======================
        // RouteLine index 1
        // ======================
        // RouteLine index 0
        // ======================

        // RouteLine can contain multiple FloorSegments:
        //
        // ======================
        // RouteLine index 2
        // - FloorSegment index 2   ---> this is currently rendered route line
        // - FloorSegment index 1
        // - FloorSegment index 0
        // ======================
        // RouteLine index 1
        // - FloorSegment index 2
        // - FloorSegment index 1
        // - FloorSegment index 0
        // ======================
        // RouteLine index 0
        // - FloorSegment index 2
        // - FloorSegment index 1
        // - FloorSegment index 0
        // ======================

        // If RouteLine and FloorSegment indices matches, it means we are rendering route line for currently selected floor.
        //
        // Examples:
        // * routeLineIndex == 0 && floorSegmentIndex == 0
        // ---> we are at bottom floor and we are rendering route line for the bottom floor
        //
        // * routeLineIndex == 0 && floorSegmentIndex == 1
        // ---> we are at the bottom floor and rendering route line for the first floor

        float alpha;
        @ColorRes int color;

        if (routeLineIndex == floorSegmentIndex) {
            alpha = 1.0f;
            color = R.color.route;

        } else {
            // using another color and alpha, just for fun
            int delta = Math.abs(routeLineIndex - floorSegmentIndex);
            if (delta == 1) {
                alpha = 0.70f;
            } else if (delta == 2) {
                alpha = 0.35f;
            } else {
                alpha = 0.10f;
            }
            color = R.color.route_2;
        }

        PropertyValue[] properties = new PropertyValue[5];
        properties[0] = PropertyFactory.lineCap(Property.LINE_CAP_ROUND);
        properties[1] = PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND);
        properties[2] = PropertyFactory.lineWidth(4f);
        properties[3] = PropertyFactory.lineColor(ContextCompat.getColor(context, color));
        properties[4] = PropertyFactory.lineOpacity(alpha);

        // dotted line is funny, you should try it!
        //properties[4] = PropertyFactory.lineDasharray(new Float[]{0.01f, 2f});

        return properties;
    }

    @Override
    public PropertyValue[] createOriginProperties(Context context, int floor) {
        PropertyValue[] properties = new PropertyValue[2];
        properties[0] = PropertyFactory.iconImage(ROUTE_PROPERTY_ICON_ORIGIN);
        properties[1] = PropertyFactory.iconAllowOverlap(true);
        return properties;
    }

    @Override
    public int getOriginDrawable() {
        return com.steerpath.sdk.R.drawable.ic_route_segment_start_point;
    }

    @Override
    public PropertyValue[] createFloorUpProperties(Context context, int floor) {
        PropertyValue[] properties = new PropertyValue[11];
        properties[0] = PropertyFactory.iconImage(ROUTE_PROPERTY_ICON_FLOOR_UP);
        properties[1] = PropertyFactory.iconAllowOverlap(true);
        properties[2] = PropertyFactory.textField(ROUTE_PROPERTY_TEXT); // see R.string.sp_route_floor_up_text
        properties[3] = PropertyFactory.textColor(ContextCompat.getColor(context, R.color.route));
        properties[4] = PropertyFactory.textSize(15f);
        properties[5] = PropertyFactory.textOffset(new Float[] {0f, 1.4f}); // move text below icon
        properties[6] = PropertyFactory.textAllowOverlap(true);
        properties[7] = PropertyFactory.textHaloColor(Color.BLACK);
        properties[8] = PropertyFactory.textHaloWidth(0.5f);
        properties[9] = PropertyFactory.textHaloBlur(0.5f);
        properties[10] = PropertyFactory.textFont(new String[] {"arial"});
        return properties;
    }

    @Override
    public int getFloorUpDrawable() {
        return com.steerpath.sdk.R.drawable.ic_sp_travel_upward;
    }

    @Override
    public PropertyValue[] createFloorDownProperties(Context context, int floor) {
        PropertyValue[] properties = new PropertyValue[11];
        properties[0] = PropertyFactory.iconImage(ROUTE_PROPERTY_ICON_FLOOR_DOWN);
        properties[1] = PropertyFactory.iconAllowOverlap(true);
        properties[2] = PropertyFactory.textField(ROUTE_PROPERTY_TEXT); // see R.string.sp_route_floor_down_text
        properties[3] = PropertyFactory.textColor(ContextCompat.getColor(context, R.color.route));
        properties[4] = PropertyFactory.textSize(15f);
        properties[5] = PropertyFactory.textOffset(new Float[] {0f, 1.4f}); // move text below icon
        properties[6] = PropertyFactory.textAllowOverlap(true);
        properties[7] = PropertyFactory.textHaloColor(Color.BLACK);
        properties[8] = PropertyFactory.textHaloWidth(0.5f);
        properties[9] = PropertyFactory.textHaloBlur(0.5f);
        properties[10] = PropertyFactory.textFont(new String[] {"arial"});
        return properties;
    }

    @Override
    public int getFloorDownDrawable() {
        return com.steerpath.sdk.R.drawable.ic_sp_travel_downward;
    }

    @Override
    public PropertyValue[] createWaypointProperties(Context context, int floor) {
        PropertyValue[] properties = new PropertyValue[2];
        properties[0] = PropertyFactory.iconImage(ROUTE_PROPERTY_ICON_WAYPOINT);
        properties[1] = PropertyFactory.iconAllowOverlap(true);
        return properties;
    }

    @Override
    public int getWaypointDrawable() {
        return com.steerpath.sdk.R.drawable.ic_route_segment_start_point;
    }

    @Override
    public PropertyValue[] createDestinationProperties(Context context, int floor) {
        PropertyValue[] properties = new PropertyValue[2];
        properties[0] = PropertyFactory.iconImage(ROUTE_PROPERTY_ICON_DESTINATION);
        properties[1] = PropertyFactory.iconAllowOverlap(true);
        return properties;
    }

    @DrawableRes
    @Override
    public int getDestinationDrawable() {
        return com.steerpath.sdk.R.drawable.ic_route_end_point;
    }
}
